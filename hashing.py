import hashlib


def name_hashing(name):
    """
    hashing user name for create unique key
    """
    print('enter your name')

    name = name.encode()
    hashed_name = hashlib.md5(name)
    hashed_name = str(hashed_name.hexdigest())

    return hashed_name
